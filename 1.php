<?php
    // 判断post是否传过来sub参数,从而判断是提交计算,还是刷新页面
    if (isset($_POST['sub'])){
        

        // 判断两个运算元是否为数字
        if (!is_numeric($_POST['num1']) || !is_numeric($_POST['num2'])){
            $isDo = false;
            echo "存在运算元不是数字，无法运算<br>";
        }
        else{
            $isDo = true;
        }
 
        // 声明变量 计算结果
        $sum = "";
 
        if ($isDo){ // 判断两个运算元是否为数字
            switch ($_POST['ysf']){
                case '+':
                    $sum = $_POST['num1'] + $_POST['num2'];
                    break;
                case '-':
                    $sum = $_POST['num1'] - $_POST['num2'];
                    break;
                case '*':
                    $sum = $_POST['num1'] * $_POST['num2'];
                    break;
                case '/':
                    $sum = $_POST['num1'] / $_POST['num2'];
                    break;
                case '%':
                    $sum = $_POST['num1'] % $_POST['num2'];
                    break;
            }
           
        }
    }
    else{
        echo "请刷新页面<br>";
    }
?>
 
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!--声明字符编码-->
    <title>简易计算器</title>
</head>
<body background="">
<table border="1" width="400" align="center" background="">
    <form action="" method="post">
    <!--action发送数据到新的表单，method以何种方式发送-->
        <tr>
            <!--第一个运算元-->
            <td><input type="text" size="5" name="num1" value="<?php
                if (isset($_POST['sub'])){echo $_POST['num1'];} ?>"></td>
            
            <!--运算符-->
            <td>
                <select name="ysf">
                    <option <?php 
                        if (isset($_POST['sub'])){if ($_POST['ysf']=="+") echo "selected";} 
                            ?> value="+"> + </option>    
                    <option <?php 
                        if (isset($_POST['sub'])){if ($_POST['ysf']=="-") echo "selected";}
                            ?> value="-"> - </option>     
                    <option <?php 
                        if (isset($_POST['sub'])){if ($_POST['ysf']=="*") echo "selected";} 
                            ?> value="*"> * </option> 
                    <option <?php 
                        if (isset($_POST['sub'])){if ($_POST['ysf']=="/") echo "selected";}
                            ?> value="/"> / </option>
                    <option <?php 
                        if (isset($_POST['sub'])){if ($_POST['ysf']=="%") echo "selected";} 
                            ?> value="%"> % </option>  
                </select>
            </td>
            
            <!--第二个运算元-->
            <td><input type="text" size="5" name="num2" value="<?php
                if (isset($_POST['sub'])){echo $_POST['num2'];} ?>"></td>
            
            <!--提交-->
            <td><input type="submit" name="sub" value="等于"></td>
        </tr>
        <tr>
            <td colspan="4">
            <!--跨四列-->
                <?php
                    if (isset($_POST['sub'])){
                        echo "计算结果：{$_POST['num1']}{$_POST['ysf']}{$_POST['num2']} = {$sum}";
                    }
                ?>
            </td>
        </tr>
    </form>
</table>
</body>
</html>
